<?php
use \Nette\Application\UI\Form;
use \Nette\Caching\Cache;

require APP_ROOT . '/../vendor/others/PHPExcel.php';

/**
 * Homepage presenter.
 */
class ExportPresenter extends BasePresenter
{
	/** @var \Nette\Caching\Cache */
	private $cache;

	public function startup() {
		parent::startup();
		$storage = new Nette\Caching\Storages\FileStorage(__DIR__ . '/../../temp');
		$this->cache = new Cache($storage);
	}

	public function actionExcel($key) {
		$templateFile = __DIR__ . '/../templates/Export/excel.xlsx';
		$objPHPExcel = new PHPExcel_Reader_Excel2007();
		$objPHPExcel = $objPHPExcel->load($templateFile);
		$objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

		$this->fillInInfo($objPHPExcel, $key);
		$this->fillInData($objPHPExcel, $key);

		$this->outputToBrowser($objPHPExcel, $key);

	}

	private function fillInData($objPHPExcel, $key) {
		$maximumEntries = 288;
		$data = $this->cache->load($key);
		$totalVolume = $data[count($data) - 1]['V'];
		$volumeAlert = floor($totalVolume * 0.95);
		if(count($data) > $maximumEntries) die('too much data!');
		$dataCols = array('B', 'E', 'H', 'K');
		$markersCols = array('A', 'D', 'G', 'J');
		$dataRows = array(
			array(
				// structure: starting_row, ending_row, index_to_break_next_row
				8, 53, 184
			), array(
				58, 83, 288
			)
		);

		$styleArray = array(
			'font'  => array(
				'color' => array('rgb' => 'FF0000'),
		));

		$currentCol = 0;
		$currentRowInfo = 0;
		$currentRow = $dataRows[$currentRowInfo][0];
		foreach($data as $i => $d) {
			if($i + 1 > $dataRows[$currentRowInfo][2]) {
				// if we got pass index break mark
				$currentRowInfo++;
				$currentRow = $dataRows[$currentRowInfo][0];
				$currentCol = 0;
			}
			if($currentRow > $dataRows[$currentRowInfo][1]) {
				// if we increased over our row limit
				$currentRow = $dataRows[$currentRowInfo][0];
				$currentCol++;
			}

			$position = $dataCols[$currentCol] . $currentRow;

			// check if volume is over 95%
			if($d['V'] >= $volumeAlert) {
				$markerPosition = $markersCols[$currentCol] . $currentRow;
				$objPHPExcel->setActiveSheetIndex(0)->getStyle($position)->applyFromArray($styleArray);
				$objPHPExcel->setActiveSheetIndex(0)->getStyle($markerPosition)->applyFromArray($styleArray);
			}


			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($position, round($d['V'], 0));
			$currentRow++;
//			print $i . ' ' .$position . '<br />';
		}

	}

	private function fillInInfo($objPHPExcel, $key) {
		$volumeInfoPosition = 'A1';
		$data = $this->cache->load($key);
		$totalVolume = $data[count($data) - 1]['V'];
		$totalVolume = $totalVolume / 1000;
		$totalVolume = round($totalVolume, 0);
		$cell = $objPHPExcel->setActiveSheetIndex(0)->getCell($volumeInfoPosition);
		$cellText = $cell->getValue();
		$cellText = str_replace('$VOLUME', $totalVolume, $cellText);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($volumeInfoPosition, $cellText);

		$timeInfoPosition = 'A2';
		$now = new \Nette\DateTime();
		$time = $now->format("d.m.Y");
		$cell = $objPHPExcel->setActiveSheetIndex(0)->getCell($timeInfoPosition);
		$cellText = $cell->getValue();
		$cellText = str_replace('$DATE', $time, $cellText);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($timeInfoPosition, $cellText);
	}

	private function outputToBrowser($objPHPExcel, $key) {
		$data = $this->cache->load($key);
		$totalVolume = $data[count($data) - 1]['V'];
		$totalVolume = $totalVolume / 1000;
		$totalVolume = round($totalVolume, 0);
		$filename = 'tabulka_' . $totalVolume . 'm3.xlsx';

		header('Pragma: public');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Content-Type: application/force-download');
		header('Content-Type: application/octet-stream');
		header('Content-Type: application/download');
		header("Content-Disposition: attachment;filename=$filename");
		header('Content-Transfer-Encoding: binary');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		die();
	}
}