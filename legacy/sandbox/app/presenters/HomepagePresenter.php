<?php
use \Nette\Application\UI\Form;
use \Nette\Caching\Cache;
/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{
	/** @var \Nette\Caching\Cache */
	private $cache;
	
	public function startup() {
		parent::startup();
		$storage = new Nette\Caching\Storages\FileStorage(__DIR__ . '/../../temp');
		$this->cache = new Cache($storage);
	}

	public function renderDefault()	{
		$registered = array(
			"laidDownCylinder" => array(
				"name" => "Laid down cylinder",
				"image" => null,
			),
			"cylinderAndBrick" => array(
				"name" => "Cylinder and brick",
				"image" => null,
			),
			"brick" => array(
				"name" => "Brick",
				"image" => null,
			),
			"eightSideCylinder" => array(
				"name" => "Eight side cylinder",
				"image" => "8side.png",
			),
			"spheroidTank" => array(
				"name" => "Spheroid tank",
				"image" => "spheroid-tank.png",
			),
			"cylinderTank" => array(
				"name" => "Cylinder tank",
				"image" => null,
			),
			"spheroidCylinderTank" => array(
				"name" => "Spheroid cylinder tank",
				"image" => "spheroid-cylinder-tank.png",
			),
		);
		$this->template->registered = $registered;
	}
	
	public function renderPreview($key) {		
		$this->template->data = $this->cache->load($key);
		$this->template->key = $key;
		
	}

	protected function createComponentSpheroidCylinderTank() {
		$form = new Form;
		$form->addText("diameter", "Priemer (parne cislo)");
		$form->addText("length_left", "Dlzka zaoblenia nalavo (L left)");
		$form->addText("length_part", "Dlzka hlavnej casti (L part)");
		$form->addText("length_right", "Dlzka zaoblenia napravo (L right)");
		$form->addText("step", "Step");
		$form->addSelect("output", "Output units", $this->getOutputArray());
		$form->addSubmit("submit", "Calculate");
		$form->onSuccess[] = $this->spheroidCylinderTankSubmitted;
		$form->setDefaults(array(
			"output" => "0.001",
			"step" => 1,
		));
		return $form;
	}

	public function spheroidCylinderTankSubmitted(Form $form) {
		$values = $form->getValues();

		$diameter = $values->diameter;
		$lengthLeft = $values->length_left;
		$lengthPart = $values->length_part;
		$lengthRight = $values->length_right;
		$step = $values->step;
		$outputType = $values->output;

		$model = new SpheroidCylinderTank($diameter, $lengthLeft, $lengthPart, $lengthRight, $step, $outputType);
		$model->calculate();
		$model->convert();

		$data = $model->getAnswer();
		$key = time();
		$this->cache->save($key, $data);
		$this->redirect("preview", $key);
	}

	protected function createComponentCylinderTank() {
		$form = new Form;
		$form->addText("diameter", "Priemer (parne cislo)");
		$form->addText("height", "Vyska");
		$form->addText("step", "Step");
		$form->addSelect("output", "Output units", $this->getOutputArray());
		$form->addSubmit("submit", "Calculate");
		$form->onSuccess[] = $this->cylinderTankSubmitted;
		$form->setDefaults(array(
			"output" => "0.001",
			"step" => 1,
		));
		return $form;
	}

	public function cylinderTankSubmitted(Form $form) {
		$values = $form->getValues();

		$diameter = $values->diameter;
		$height = $values->height;
		$step = $values->step;
		$outputType = $values->output;

		$model = new CylinderTank($diameter, $height, $step, $outputType);
		$model->calculate();
		$model->convert();

		$data = $model->getAnswer();
		$key = time();
		$this->cache->save($key, $data);
		$this->redirect("preview", $key);
	}

	protected function createComponentSpheroidTank() {
		$form = new Form;
		$form->addText("height_top", "Vyska horneho zaoblenia (H top)");
		$form->addText("height_part", "Vyska hlavnej casti (H part)");
		$form->addText("height_bottom", "Vyska spodneho zablenia (H bottom)");
		$form->addText("width_total", "Sirka (W total)");
		$form->addText("length_left", "Dlzka zaoblenia nalavo (L left)");
		$form->addText("length_part", "Dlzka hlavnej casti (L part)");
		$form->addText("length_right", "Dlzka zaoblenia napravo (L right)");
		$form->addText("step", "Step");
		$form->addSelect("output", "Output units", $this->getOutputArray());
		$form->addSubmit("submit", "Calculate");
		$form->onSuccess[] = $this->spheroidTankSubmitted;
		$form->setDefaults(array(
			"output" => "0.001",
			"step" => 1,
		));
		return $form;
	}

	public function spheroidTankSubmitted(Form $form) {
		$values = $form->getValues();

		$heightTop = $values->height_top;
		$heightPart = $values->height_part;
		$heightBottom = $values->height_bottom;
		$width = $values->width_total;
		$lengthLeft = $values->length_left;
		$lengthPart = $values->length_part;
		$lengthRight = $values->length_right;
		$step = $values->step;
		$outputType = $values->output;

		$model = new SpheroidTank($heightTop, $heightPart, $heightBottom, $width, $lengthLeft, $lengthPart, $lengthRight, $step, $outputType);
		$model->calculate();
		$model->convert();

		$data = $model->getAnswer();
		$key = time();
		$this->cache->save($key, $data);
		$this->redirect("preview", $key);
	}

	protected function createComponentEightSideCylinder() {
		$form = new Form;
		$form->addText("height_total", "Plna vyska (H)");
		$form->addText("height_partial", "Ciastocna vyska trapezoidu  (Ht)");
		$form->addText("width_total", "Plna sirka (W)");
		$form->addText("width_partial", "Ciastocna sirka (Wt)");
		$form->addText("length", "Dlzka (L)");
		$form->addText("step", "Step");
		$form->addSelect("output", "Output units", $this->getOutputArray());
		$form->addSubmit("submit", "Calculate");
		$form->onSuccess[] = $this->eightSideCylinderSubmitted;
		$form->setDefaults(array(
			"output" => "0.001",
			"bonusHeight" => 0,
			"bonusDiameter" => 0,
			"step" => 1,
		));
		return $form;
	}

	public function eightSideCylinderSubmitted(Form $form) {
		$values = $form->getValues();

		$widthTotal = $values->width_total;
		$widthPartial = $values->width_partial;
		$heightTotal = $values->height_total;
		$heightPartial = $values->height_partial;
		$step = $values->step;
		$length = $values->length;
		$output = $values->output;

		$model = new EightSideCylinder($heightTotal, $heightPartial, $widthTotal, $widthPartial, $length, $step, $output);
		$model->calculate();
		$model->convert();

		$data = $model->getAnswer();
		$key = time();
		$this->cache->save($key, $data);
		$this->redirect("preview", $key);
	}
	
	protected function createComponentLaidDownCylinder() {
		$form = new Form;
		$form->addText("diameter", "Diameter (even number)");
		$form->addText("length", "Length");
		$form->addText("bonusHeight", "Kalnik vyska");
		$form->addText("bonusDiameter", "Kalnik diameter");
		$form->addText("step", "Step");
		$form->addSelect("output", "Output units", $this->getOutputArray());
		$form->addSubmit("submit", "Calculate");
		$form->onSuccess[] = $this->laidDownCylinderSubmitted;
		$form->setDefaults(array(
			"output" => "0.001",
			"bonusHeight" => 0,
			"bonusDiameter" => 0,
			"step" => 1,
		));
		return $form;
	}
	
	public function laidDownCylinderSubmitted(Form $form) {
		$values = $form->getValues();
		
		$radius = $values->diameter / 2;
		$length = $values->length;
		$step = $values->step;
		$outputUnit = $values->output;
		$model = new CircleSegment($radius, $length, $step, $outputUnit);
		$model->setBonus($values->bonusDiameter, $values->bonusHeight);
		$model->calculate();
		$model->convert();
		
		$data = $model->getAnswer();
		$key = time();
		$this->cache->save($key, $data);
		$this->redirect("preview", $key);
	}
	
	protected function createComponentCylinderAndBrick() {
		$form = new Form;
		$form->addText("height", "Height");
		$form->addText("width", "Width");
		$form->addText("length", "Length");
		$form->addText("arcHeight", "Arch height");
		$form->addText("bonusHeight", "Kalnik vyska");
		$form->addText("bonusDiameter", "Kalnik diameter");
		$form->addText("step", "Step");
		$form->addSelect("output", "Output units", $this->getOutputArray());
		$form->addSubmit("submit", "Calculate");
		$form->onSuccess[] = $this->cylinderAndBrickSubmitted;
		$form->setDefaults(array(
			"output" => "0.001",
			"bonusHeight" => 0,
			"bonusDiameter" => 0,
			"step" => 1,
		));
		return $form;
	}
	
	public function cylinderAndBrickSubmitted(Form $form) {
		$values = $form->getValues();
		
		$height = $values->height;
		$width = $values->width;
		$length = $values->length;
		$arcHeight = $values->arcHeight;
		$bonusHeight = $values->bonusHeight;
		$bonusDiameter = $values->bonusDiameter;
		$step = $values->step;
		$outputUnit = $values->output;
		
		$model = new CubeAndCircleSegment($height, $width, $length, $arcHeight, $step, $outputUnit);
		$model->setBonus($bonusDiameter, $bonusHeight);
		$model->calculate();
		$model->convert();
		
		$data = $model->getAnswer();
		$key = time();
		$this->cache->save($key, $data);
		$this->redirect("preview", $key);
	}
	
	protected function createComponentBrick() {
		$form = new Form;
		$form->addText("height", "Height");
		$form->addText("width", "Width");
		$form->addText("length", "Length");
		$form->addText("step", "Step");
		$form->addSelect("output", "Output units", $this->getOutputArray());
		$form->addSubmit("submit", "Calculate");
		$form->onSuccess[] = $this->brickSubmitted;
		$form->setDefaults(array(
			"step" => 1,
			"output" => "0.001")
		);
		return $form;
	}

	public function brickSubmitted(Form $form) {
		$values = $form->getValues();

		$height = $values->height;
		$width = $values->width;
		$length = $values->length;
		$step = $values->step;
		$outputUnit = $values->output;

		$model = new cubeSegment($height, $length, $width, $step, $outputUnit);
		$model->calculate();
		$model->convert();

		$data = $model->getAnswer();
		$key = time();
		$this->cache->save($key, $data);
		$this->redirect("preview", $key);
	}
	
	private function getOutputArray() {
		$out = array();
		$out["0.000000000001"] = "km3";
		$out["0.000001"] = "hm3";
		$out["0.000001"] = "m3";
		$out["0.001"] = "dm3 / l";
		$out["1"] = "cm3";
		$out["1000"] = "mm3";
		return $out;
	}

}
