<?php
class SpheroidCylinderTank extends Nette\Object {
	private $radius;
	private $lenghtTotal;
	private $lengthLeft;
	private $lengthPart;
	private $lengthRight;
	private $step;
	private $outputType;
	private $answer = array();

	function __construct($diameter, $lengthLeft, $lengthPart, $lengthRight, $step, $outputType) {
		$this->radius = $diameter / 2;
		$this->lengthLeft = $lengthLeft;
		$this->lengthPart = $lengthPart;
		$this->lengthRight = $lengthRight;
		$this->step = $step;
		$this->outputType = $outputType;

		$this->lenghtTotal = $lengthLeft + $lengthPart + $lengthRight;

//		die(print_r($this->sphere));
	}

	public function getAnswer() {
		return $this->answer;
	}
	
	public function convert() {
		$this->convertToAnotherType($this->outputType);
	}
	
	function convertToAnotherType($type) {
		for($i = 0; $i < count($this->answer); $i++)
			$this->answer[$i]['V'] *= $type;			
	}

	function calculate() {
		$step = $this->step;

		$volume = 0;
		// laid down cylinder
		$cylinderModel = new CircleSegment($this->radius, $this->lengthPart, $this->step, $this->outputType);
		$cylinderModel->calculate();
		$cylinderVolume = $cylinderModel->getAnswer();
//		die(print_r($cylinderVolume));

		for($i = 1; $i <= $this->radius * 2; $i = $i + $step) {
			$volume = $cylinderVolume[$i - 1]['V'];

			if($this->lengthLeft > 0) {
				$volume += $this->calculateElipsoidVolumeAtHeight($i, $this->lengthLeft);
			}

			if($this->lengthRight > 0) {
				$volume += $this->calculateElipsoidVolumeAtHeight($i, $this->lengthRight);
			}

			$this->answer[$i - 1] = array(
				"h" => $i,
				"V" => $volume,
			);
		}
//		die(print_r($milestones));
		// remove first item

	}

	function search($products, $field, $value) {
		foreach($products as $key => $product) {
			if ( $product[$field] === $value )
				return $key;
		}
		return false;
	}

	/**
	 * @param $height
	 * 2x elipsoid = spheroid
	 */
	public function calculateElipsoidVolumeAtHeight($height, $smallRadius) {
		$R = $this->radius;
		$r = $smallRadius;
		$y = $height;

//		die(print "R: " . $R . ' r: ' . $r . ' y: '.$y);

		$spheroidVolume = (pi() * $r * (3 * $R - $y) * pow($y, 2)) / (3 * $R);
		$elipsoidVolume = $spheroidVolume / 2;

		return $elipsoidVolume;
	}
}

?>