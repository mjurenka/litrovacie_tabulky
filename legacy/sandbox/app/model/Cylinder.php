<?php
class CylinderTank extends Nette\Object {
	private $radius;
	private $heigth;
	private $step;
	private $outputType;
	private $answer = array();

	function __construct($diameter, $height, $step, $outputType)
	{
		$this->radius = $diameter / 2;
		$this->heigth = $height;
		$this->step = $step;
		$this->outputType = $outputType;
	}
		
	public function getAnswer() {
		return $this->answer;
	}
	
	public function convert() {
		$this->convertToAnotherType($this->outputType);
	}
	
	function convertToAnotherType($type) {
		for($i = 0; $i < count($this->answer); $i++)
			$this->answer[$i]['V'] *= $type;			
	}

	function calculate() {
		$step = $this->step;
		for($i = 0; $i <= $this->heigth; $i = $i + $step) {
			$this->answer[$i] = array(
				"h" => $i,
				"V" => $this->calculateVolumeCylinder($i),
			);
		}

		// remove first item
		array_shift($this->answer);
	}
	
	public function calculateVolumeCylinder($height) {
		return (pi() * pow($this->radius, 2) * $height);
	}
}
?>