<?php
class EightSideCylinder extends Nette\Object {
	private $heightTotal;
	private $heightPartial;
	private $widthTotal;
	private $widthPartial;
	private $length;
	private $step;
	private $outputType;
	private $answer = array();
	private $trapezoid = array(
		"angle" => 0,
		"area" => array(),
	);

	function __construct($heightTotal, $heightPartial, $widthTotal, $widthPartial, $length, $step, $outputType)	{
		$this->widthPartial = $widthPartial;
		$this->widthTotal = $widthTotal;
		$this->heightPartial = $heightPartial;
		$this->heightTotal = $heightTotal;
		$this->length = $length;
		$this->step = $step;
		$this->outputType = $outputType;
		$this->calculateAngle();
		$this->calculateTrapezoidData();
	}
		
	public function getAnswer() {
		return $this->answer;
	}
	
	public function convert() {
		$this->convertToAnotherType($this->outputType);
	}
	
	function convertToAnotherType($type) {
		for($i = 0; $i < count($this->answer); $i++)
			$this->answer[$i]['V'] *= $type;			
	}

	function calculate() {
		$step = $this->step;
		$pos = 0;
		$volume = 0;
		$milestones = array(
			"trapezoid" => 0,
			"brick" => 0,
		);
		for($i = 0; $i <= $this->heightTotal; $i = $i + $step) {
			$volume = 0;
			if($i <= $this->heightPartial) {
				// inverse trapezoid
				$volume = $this->getTrapezoidAreaAtHeight($i, true) * $this->length;
			}

			if($i > $this->heightPartial && $i <= $this->heightTotal - $this->heightPartial) {
				// block
//				die(print_r($i));
				$trapezoidVolume = $milestones['trapezoid'];
				$brickVolume = $this->getBrickVolumeAtHeight($i - $this->heightPartial);
				$volume = $trapezoidVolume + $brickVolume;
			}

			if($i > $this->heightTotal - $this->heightPartial) {
				// normal trapezoid
				$brickVolume = $milestones['brick'];
				$trapezoidHeight = $i - $this->heightTotal + $this->heightPartial;
				$trapezoidVolume = $this->getTrapezoidAreaAtHeight($trapezoidHeight, false) * $this->length;
				$volume = $trapezoidVolume + $brickVolume;
			}
			$data = array(
				'h' => $i,
				'V' => $volume,
			);
			$this->answer[] = $data;

			if($i == $this->heightPartial) $milestones['trapezoid'] = $volume;
			if($i == $this->heightTotal - $this->heightPartial) $milestones['brick'] = $volume;
		}
		// remove first item
		array_shift($this->answer);
	}
	
	function calculateAngle() {
		$hypotenuse = (sqrt(pow($this->heightPartial, 2) + pow($this->widthPartial, 2)));
		$angle = acos($this->widthPartial / $hypotenuse);
		$this->trapezoid['angle'] = $angle;
	}

	function calculateTrapezoidData() {
		$angle = $this->trapezoid['angle'];
		for($i = 0; $i <= $this->heightPartial; $i++) {
			$sideWidth = $i * cos($angle) / sin($angle);
			$area = ($this->widthTotal + ($this->widthTotal - 2 * ($sideWidth))) * $i;
			$area /= 2;
			$this->trapezoid['area'][$i] = $area;
		}
//		die(print_r(($this->trapezoid)));
	}

	function getTrapezoidAreaAtHeight($height, $inverseTrapezoid = false) {
		if(!$inverseTrapezoid) {
			return $this->trapezoid['area'][$height];
		} else {
			return $this->trapezoid['area'][count($this->trapezoid['area']) - 1] - $this->getTrapezoidAreaAtHeight($this->heightPartial - $height);
		}
	}

	function getBrickVolumeAtHeight($height) {
		return ($this->widthTotal) * $this->length * $height;
	}


	

}

?>