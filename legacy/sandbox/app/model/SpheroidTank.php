<?php
class SpheroidTank extends Nette\Object {
	private $heightTotal;
	private $heightTop;
	private $heightPart;
	private $heightBottom;
	private $width;
	private $lenghtTotal;
	private $lengthLeft;
	private $lengthPart;
	private $lengthRight;
	private $step;
	private $outputType;
	private $answer = array();
	private $sphere = array(
		"top" => array(
			"R" => 0,
		),
		"bottom" => array(
			"R" => 0,
		),
	);

	function __construct($heightTop, $heightPart, $heightBottom, $widthTotal, $lengthLeft, $lengthPart, $lengthRight, $step, $outputType) {
		$this->heightTop = $heightTop;
		$this->heightPart = $heightPart;
		$this->heightBottom = $heightBottom;
		$this->width = $widthTotal;
		$this->lengthLeft = $lengthLeft;
		$this->lengthPart = $lengthPart;
		$this->lengthRight = $lengthRight;
		$this->step = $step;
		$this->outputType = $outputType;

		$this->heightTotal = $heightBottom + $heightTop + $heightPart;
		$this->lenghtTotal = $lengthLeft + $lengthPart + $lengthRight;

		if($heightTop > 0) {
			$this->sphere['top']['R'] = $this->calculateSphereRadiusFromCircularSegment($lengthPart, $heightTop);
		}

		if($heightBottom > 0) {
			$this->sphere['bottom']['R'] = $this->calculateSphereRadiusFromCircularSegment($lengthPart, $heightBottom);
		}

//		die(print_r($this->sphere));
	}

	public function calculateSphereRadiusFromCircularSegment($chordLength, $segmentHeight) {
//		die(print $chordLength . ' ' . $segmentHeight);
		return ($segmentHeight / 2) + (pow($chordLength, 2) / (8 * $segmentHeight));
	}
		
	public function getAnswer() {
		return $this->answer;
	}
	
	public function convert() {
		$this->convertToAnotherType($this->outputType);
	}
	
	function convertToAnotherType($type) {
		for($i = 0; $i < count($this->answer); $i++)
			$this->answer[$i]['V'] *= $type;			
	}

	function calculate() {
		$step = $this->step;
		$pos = 0;
		$volume = 0;
		$milestones = array(
			"bottom" => 0,
			"brick" => 0,
		);
		for($i = 0; $i <= $this->heightTotal; $i = $i + $step) {
			$volume = 0;
			if($i > $this->heightBottom && $i <= $this->heightTotal - $this->heightTop)  {
				$volume = $milestones['bottom'];
//				die(print $volume);
			} elseif($i > $this->heightTotal - $this->heightTop) {
				$volume = $milestones['brick'];
//				die(print $i . ' ' . $volume);
			}


			if($i <= $this->heightBottom) {
				// bottom sphere cup
				$volume += $this->calculateSphericalSegmentVolumeAtHeight($i, $this->sphere['bottom']['R']);

			}
			if($i > $this->heightBottom && $i <= $this->heightTotal - $this->heightTop) {
				// midsection
				$volume += $this->getBrickVolumeAtHeight($i - $this->heightBottom);
			}
			if ($i > $this->heightTotal - $this->heightTop) {
				// top sphere
				$fullVolume = $this->calculateSphericalSegmentVolumeAtHeight($this->heightTop, $this->sphere['top']['R']);
				$subtract = $this->calculateSphericalSegmentVolumeAtHeight($this->heightTotal - $i, $this->sphere['top']['R']);
				$diff = $fullVolume - $subtract;
				$volume += $diff;

//				if($i == 111) die(print $volume);
			}

			// left elipsoid
			if($i > $this->heightBottom && $i <= $this->heightTotal - $this->heightTop && $this->lengthLeft > 0) {
				$tempVol = $this->calculateElipsoidVolumeAtHeight($i - $this->heightTop, $this->lengthLeft);
				$volume += $tempVol;
//				die(print $tempVol);
			}

			// right elipsoid
			if($i > $this->heightBottom && $i <= $this->heightTotal - $this->heightTop && $this->lengthRight > 0) {
				$volume += $this->calculateElipsoidVolumeAtHeight($i - $this->heightTop, $this->lengthRight);
			}

			if($i == $this->heightBottom) $milestones['bottom'] = $volume;
			if($i == $this->heightTotal - $this->heightTop) {
				$milestones['brick'] = $volume;
//				$milestones['brick'] = $milestones['bottom'] + $this->getBrickVolumeAtHeight($this->heightPart) + $this->calculateElipsoidVolumeAtHeight($this->heightPart, $this->lengthLeft) + $this->calculateElipsoidVolumeAtHeight($this->heightPart, $this->lengthRight);
//				die(print $volume . ' ' . $i);

			}

			$this->answer[$i] = array(
				"h" => $i,
				"V" => $volume,
			);
		}
//		die(print_r($milestones));
		// remove first item
		array_shift($this->answer);
	}

	/**
	 * @param $height
	 * 2x elipsoid = spheroid
	 */
	public function calculateElipsoidVolumeAtHeight($height, $smallRadius) {
		$R = $this->heightTotal;
		$r = $smallRadius;
		$y = $height;

//		die(print "R: " . $R . ' r: ' . $r . ' y: '.$y);

		$spheroidVolume = (pi() * $r * (3 * $R - $y) * pow($y, 2)) / (3 * $R);
		$elipsoidVolume = $spheroidVolume / 2;

		return $elipsoidVolume;
	}

	public function calculateSphericalSegmentVolumeAtHeight($height, $fullRadius) {
//		die(print 'h: ' . $height . ' radius: ' . $fullRadius);
		$c = $this->calculateSegmentLengthOfSphereAtHeight($height, $fullRadius);
		$h = $height;

		return pi() * $h * (pow($c, 2) / 8 + pow($h, 2) /6);
	}

	public function calculateSegmentLengthOfSphereAtHeight($height, $fullRadius) {
		$R = $fullRadius;
		$theta = $this->calculateTheta($height, $fullRadius);
		$cosTheta = cos($theta);

		return $R * sqrt(2 - 2 * $cosTheta);
	}

	public function calculateTheta($height, $fullRadius) {
		$R = $fullRadius;
		$d = $R - $height;

		return 2 * acos($d / $R);
	}



	function getBrickVolumeAtHeight($height) {
		return ($this->width) * $this->lenghtTotal * $height;
	}


	

}

?>