import math

class CircleSegment:
    def __init__(self, radius, length, step, outputType):
        self.answer = []
        self.bonusHeigth = 0
        self.bonusRadius = 0
        self.bonusVolume = 0

        self.radius = radius
        self.length = length
        self.heigth = 2 * radius
        self.step = step
        self.outputType = outputType

    def getAnswer(self):
        return self.answer
    
    def convert(self):
        self.convertToAnotherType(self.outputType)
    
    def convertToAnotherType(self, type):
        for item in self.answer:
            item['V'] *= type

    def setBonus(self, diameter, heigth):
        self.bonusHeigth = heigth
        self.bonusRadius = diameter / 2
        self.bonusVolume = self.calculateVolumeCylinder(self.bonusRadius, self.bonusHeigth, self.outputType)

    def calculate(self):
        step = self.step
        pos = 0

        i = 0
        while i <= self.radius:
            self.answer.append({
                'h': i,
                'V': self.calculateVolume(i)
            })
            pos += 1

            i += step

        i = 1
        while i <= self.radius:
            middle = self.answer[int(self.radius)]['V']
            mirror = self.answer[int(self.radius) - i]['V']
            diff = middle - mirror
            V = middle + diff

            self.answer.append({
                'h': self.radius + i,
                'V': V
            })
            pos += 1

            i += step

        # bonus volume
        for item in self.answer:
            item['h'] += self.bonusHeigth
            item['V'] += self.bonusVolume

        # remove first item
        self.answer.pop(0)

        i = self.bonusHeigth
        while i > 0:
            data = {
                'h': i,
                'V': self.bonusVolume / self.bonusHeigth * i
            }
            self.answer.insert(0, data)

            i -= step

    # @h - height of the segment
    def calculateVolume(self, h):
        return (self.calculateArea(h) * self.length)
    
    # @h = height of the segment
    def calculateArea(self, h):
        R = self.radius
        # central angle
        C = self.calculateCentralAngle(h)
        
        ans = (math.pow(R, 2) / 2) * (C - math.sin(C))
        
        return ans
    
    # @h = height of the segment
    def calculateCentralAngle(self, h):
        R = self.radius
        
        # calculate in radians
        ans = 2 * math.acos( (R - h) / R)
        
        return ans

    def calculateTotalVolume(self):
        return (math.pi * self.radius * self.radius * self.length * self.outputType)

    def calculateVolumeCylinder(self, radius, height, outputUnit):
        return (math.pi * radius * radius * height)