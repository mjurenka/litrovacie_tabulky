import math

class EightSideCylinder:
    def __init__(self, heightTotal, heightPartial, widthTotal, widthPartial, length, step, outputType):
        self.answer = []
        self.trapezoid = {
            "angle": 0,
            "area": {}
        }

        self.widthPartial = widthPartial
        self.widthTotal = widthTotal
        self.heightPartial = heightPartial
        self.heightTotal = heightTotal
        self.length = length
        self.step = step
        self.outputType = outputType
        self.calculateAngle()
        self.calculateTrapezoidData()


    def getAnswer(self):
        return self.answer
    
    def convert(self):
        self.convertToAnotherType(self.outputType)
    
    def convertToAnotherType(self, type):
        for item in self.answer:
            item['V'] *= type

    def calculate(self):
        step = self.step
        pos = 0
        volume = 0
        milestones = {
            "trapezoid": 0,
            "brick": 0,
        }

        i = 0
        while i <= self.heightTotal:
            volume = 0
            if i <= self.heightPartial:
                # inverse trapezoid
                volume = self.getTrapezoidAreaAtHeight(i, True) * self.length

            if i > self.heightPartial and i <= self.heightTotal - self.heightPartial:
                # block
                trapezoidVolume = milestones['trapezoid']
                brickVolume = self.getBrickVolumeAtHeight(i - self.heightPartial)
                volume = trapezoidVolume + brickVolume

            if i > self.heightTotal - self.heightPartial:
                # normal trapezoid
                brickVolume = milestones['brick']
                trapezoidHeight = i - self.heightTotal + self.heightPartial
                trapezoidVolume = self.getTrapezoidAreaAtHeight(trapezoidHeight, False) * self.length
                volume = trapezoidVolume + brickVolume
            
            data = {
                'h': i,
                'V': volume,
            }
            self.answer.append(data)

            if i == self.heightPartial:
                milestones['trapezoid'] = volume
            
            if i == self.heightTotal - self.heightPartial:
                milestones['brick'] = volume

            i += step

        # remove first item
        self.answer.pop(0)
    
    def calculateAngle(self):
        hypotenuse = (math.sqrt(pow(self.heightPartial, 2) + math.pow(self.widthPartial, 2)))
        angle = math.acos(self.widthPartial / hypotenuse)
        self.trapezoid['angle'] = angle

    def calculateTrapezoidData(self):
        angle = self.trapezoid['angle']
        i = 0
        while i <= self.heightPartial:
            sideWidth = i * math.cos(angle) / math.sin(angle)
            area = (self.widthTotal + (self.widthTotal - 2 * (sideWidth))) * i
            area /= 2
            self.trapezoid['area'][i] = area

            i += 1

    def getTrapezoidAreaAtHeight(self, height, inverseTrapezoid = False):
        if not inverseTrapezoid:
            return self.trapezoid['area'][height]
        else:
            return self.trapezoid['area'][len(self.trapezoid['area']) - 1] - self.getTrapezoidAreaAtHeight(self.heightPartial - height)

    def getBrickVolumeAtHeight(self, height):
        return (self.widthTotal) * self.length * height