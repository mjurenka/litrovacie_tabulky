import math

class CubeAndCircleSegment:
    def __init__(self, height, width, depth, arcHeigth, step, outputType):
        self.answer = []
        self.bonusHeigth = 0
        self.bonusRadius = 0
        self.bonusVolume = 0

        self.height = height
        self.width = width
        self.depth = depth
        self.arcHeigth = arcHeigth
        self.radius = self.calculateRadiusOfArc(arcHeigth, width)
        self.step = step
        self.outputType = outputType

    def getAnswer(self):
        return self.answer
    
    def convert(self):
        self.convertToAnotherType(self.outputType)
    
    def convertToAnotherType(self, type):
        for item in self.answer:
            item['V'] *= type

    def setBonus(self, diameter, heigth):
        self.bonusHeigth = heigth
        self.bonusRadius = diameter / 2
        self.bonusVolume = self.calculateVolumeCylinder(self.bonusRadius, self.bonusHeigth, self.outputType)
    
    def calculateVolumeCylinder(self, radius, height, outputUnit):
        return (math.pi * radius * radius * height)
    
    def calculateRadiusOfArc(self, arcHeigth, arcWidth):
        r = arcHeigth / 2
        r += (math.pow(arcWidth, 2) / (8 * arcHeigth))
        return r

    def calculate(self):
        # 0 - arcHeigth : calculate vol of segment
        # arcHeigth - half: calculate volume of brick
        step = self.step
        pos = 0
        halfHeight = self.height / 2
        maxVolArc = 0
        
        i = 0
        while i <= halfHeight:
            if i <= self.arcHeigth:
                V = self.calculateVolumeArc(i)
                maxVolArc = V
            else:
                V = maxVolArc + self.calculateVolumeBrick(i - self.arcHeigth)

            self.answer.append({
                'h': i,
                'V': V,
            })
            pos += 1

            i += step

        i = 1
        while i <= halfHeight:
            middle = self.answer[halfHeight]['V']
            mirror = self.answer[halfHeight - i]['V']
            diff = middle - mirror
            V = middle + diff

            self.answer.append({
                'h': halfHeight + i,
                'V': V
            })
            pos += 1

            i += step

        # bonus volume
        for item in self.answer:
            item['h'] += self.bonusHeigth
            item['V'] += self.bonusVolume

        # remove first item
        self.answer.pop(0)

        i = self.bonusHeigth
        while i > 0:
            data = {
                'h': i,
                'V': self.bonusVolume / self.bonusHeigth * i
            }
            self.answer.insert(0, data)

            i -= step

    # @h - height of the segment
    def calculateVolumeArc(self, h):
        return (self.calculateArea(h) * self.depth)
    
    def calculateVolumeBrick(self, h):
        return self.width * self.depth * h
    
    # @h = height of the segment
    def calculateArea(self, h):
        R = self.radius
        # central angle
        C = self.calculateCentralAngle(h)

        ans = (math.pow(R, 2) / 2) * (C - math.sin(C))

        return ans
    
    # @h = height of the segment
    def calculateCentralAngle(self, h):
        R = self.radius
        
        # calculate in radians
        ans = 2 * math.acos( (R - h) / R)
        
        return ans
    
    def calculateTotalVolume(self):
        return 0