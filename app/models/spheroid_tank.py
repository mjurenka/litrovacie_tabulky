import math

class SpheroidTank:

    def __init__(self, heightTop, heightPart, heightBottom, widthTotal, lengthLeft, lengthPart, lengthRight, step, outputType):
        self.answer = []
        self.bonusHeigth = 0
        self.bonusVolume = 0

        self.sphere = {
            "top": {
                "R": 0
            },
            "bottom": {
                "R": 0,
            }
        }

        self.heightTop = heightTop
        self.heightPart = heightPart
        self.heightBottom = heightBottom
        self.width = widthTotal
        self.lengthLeft = lengthLeft
        self.lengthPart = lengthPart
        self.lengthRight = lengthRight
        self.step = step
        self.outputType = outputType

        self.heightTotal = heightBottom + heightTop + heightPart
        self.lenghtTotal = lengthLeft + lengthPart + lengthRight

        if heightTop > 0:
            self.sphere['top']['R'] = self.calculateSphereRadiusFromCircularSegment(lengthPart, heightTop)

        if heightBottom > 0:
            self.sphere['bottom']['R'] = self.calculateSphereRadiusFromCircularSegment(lengthPart, heightBottom)


    def calculateSphereRadiusFromCircularSegment(self, chordLength, segmentHeight):
        return (segmentHeight / 2) + (math.pow(chordLength, 2) / (8 * segmentHeight))
        
    def getAnswer(self):
        return self.answer
    
    def convert(self):
        self.convertToAnotherType(self.outputType)
    
    def convertToAnotherType(self, type):
        for item in self.answer:
            item['V'] *= type

    def setBonus(self, height, volume):
        self.bonusHeigth = height
        self.bonusVolume = volume


    def calculate(self):
        step = self.step
        pos = 0
        volume = 0
        milestones = {
            "bottom": 0,
            "brick": 0,
        }
        i = 0
        while i <= self.heightTotal:
            volume = 0
            if i > self.heightBottom and i <= self.heightTotal - self.heightTop:
                volume = milestones['bottom']
            elif i > self.heightTotal - self.heightTop:
                volume = milestones['brick']

            if i <= self.heightBottom:
                # bottom sphere cup
                volume += self.calculateSphericalSegmentVolumeAtHeight(i, self.sphere['bottom']['R'])

            if i > self.heightBottom and i <= self.heightTotal - self.heightTop:
                # midsection
                volume += self.getBrickVolumeAtHeight(i - self.heightBottom)
            
            if i > self.heightTotal - self.heightTop:
                # top sphere
                fullVolume = self.calculateSphericalSegmentVolumeAtHeight(self.heightTop, self.sphere['top']['R'])
                subtract = self.calculateSphericalSegmentVolumeAtHeight(self.heightTotal - i, self.sphere['top']['R'])
                diff = fullVolume - subtract
                volume += diff

            # left elipsoid
            if i > self.heightBottom and i <= self.heightTotal - self.heightTop and self.lengthLeft > 0:
                tempVol = self.calculateElipsoidVolumeAtHeight(i - self.heightTop, self.lengthLeft)
                volume += tempVol

            # right elipsoid
            if i > self.heightBottom and i <= self.heightTotal - self.heightTop and self.lengthRight > 0:
                volume += self.calculateElipsoidVolumeAtHeight(i - self.heightTop, self.lengthRight)

            if i == self.heightBottom:
                milestones['bottom'] = volume
            
            if i == self.heightTotal - self.heightTop:
                milestones['brick'] = volume

            self.answer.append({
                "h": i,
                "V": volume,
            })

            i += step

        # bonus volume
        for item in self.answer:
            item['h'] += self.bonusHeigth
            item['V'] += self.bonusVolume

        # remove first item
        self.answer.pop(0)

        i = self.bonusHeigth
        while i > 0:
            data = {
                'h': i,
                'V': self.bonusVolume / self.bonusHeigth * i
            }
            self.answer.insert(0, data)

            i -= step

    # /**
    #  * @param height
    #  * 2x elipsoid = spheroid
    #  */
    def calculateElipsoidVolumeAtHeight(self, height, smallRadius):
        R = self.heightTotal
        r = smallRadius
        y = height

        spheroidVolume = (math.pi * r * (3 * R - y) * math.pow(y, 2)) / (3 * R)
        elipsoidVolume = spheroidVolume / 2

        return elipsoidVolume

    def calculateSphericalSegmentVolumeAtHeight(self, height, fullRadius):
        c = self.calculateSegmentLengthOfSphereAtHeight(height, fullRadius)
        h = height

        return math.pi * h * (math.pow(c, 2) / 8 + math.pow(h, 2) /6)

    def calculateSegmentLengthOfSphereAtHeight(self, height, fullRadius):
        R = fullRadius
        theta = self.calculateTheta(height, fullRadius)
        cosTheta = math.cos(theta)

        return R * math.sqrt(2 - 2 * cosTheta)

    def calculateTheta(self, height, fullRadius):
        R = fullRadius
        d = R - height

        return 2 * math.acos(d / R)

    def getBrickVolumeAtHeight(self, height):
        return (self.width) * self.lenghtTotal * height