import math

class CylinderTank:
    def __init__(self, diameter, height, step, outputType):
        self.answer = []
        self.radius = diameter / 2
        self.heigth = height
        self.step = step
        self.outputType = outputType
        
    def getAnswer(self):
        return self.answer
    
    def convert(self):
        self.convertToAnotherType(self.outputType)
    
    def convertToAnotherType(self, type):
        for item in self.answer:
            item['V'] *= type

    def calculate(self):
        step = self.step

        i = 0
        while i <= self.heigth:
            self.answer.append({
                "h": i,
                "V": self.calculateVolumeCylinder(i),
            })

            i += step

        # remove first item
        self.answer.pop(0)
    
    def calculateVolumeCylinder(self, height):
        return (math.pi * math.pow(self.radius, 2) * height)