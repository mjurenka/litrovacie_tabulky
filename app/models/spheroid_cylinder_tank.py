import math

from .circle_segment import CircleSegment

class SpheroidCylinderTank:

    def __init__(self, *, diameter, lengthLeft, lengthPart, lengthRight, step, outputType):
        self.answer = []
        self.radius = diameter / 2
        self.lengthLeft = lengthLeft
        self.lengthPart = lengthPart
        self.lengthRight = lengthRight
        self.step = step
        self.outputType = outputType

        self.lenghtTotal = lengthLeft + lengthPart + lengthRight

    def getAnswer(self):
        return self.answer
    
    def convert(self):
        self.convertToAnotherType(self.outputType)
    
    def convertToAnotherType(self, type):
        for item in self.answer:
            item['V'] *= type
    
    def calculate(self):
        step = self.step

        volume = 0
        # laid down cylinder
        cylinderModel = CircleSegment(self.radius, self.lengthPart, self.step, self.outputType)
        cylinderModel.calculate()
        cylinderVolume = cylinderModel.getAnswer()
        
        i = 1
        while i <= self.radius * 2:
            volume = cylinderVolume[i - 1]['V']

            if self.lengthLeft > 0:
                volume += self.calculateElipsoidVolumeAtHeight(i, self.lengthLeft)

            if self.lengthRight > 0:
                volume += self.calculateElipsoidVolumeAtHeight(i, self.lengthRight)

            self.answer.append({
                "h": i,
                "V": volume,
            })

            i += step

    # /**
    #  * @param height
    #  * 2x elipsoid = spheroid
    #  */
    def calculateElipsoidVolumeAtHeight(self, height, smallRadius):
        R = self.radius
        r = smallRadius
        y = height

        spheroidVolume = (math.pi * r * (3 * R - y) * math.pow(y, 2)) / (3 * R)
        elipsoidVolume = spheroidVolume / 2

        return elipsoidVolume