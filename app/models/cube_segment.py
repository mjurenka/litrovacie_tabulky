import math

class CubeSegment:
    def __init__(self, heigth, length, width, step, outputType):
        self.answer = []
        self.bonusHeigth = 0
        self.bonusRadius = 0
        self.bonusVolume = 0

        self.width = width
        self.length = length
        self.heigth = heigth
        self.step = step
        self.outputType = outputType

    def getAnswer(self):
        return self.answer
    
    def convert(self):
        self.convertToAnotherType(self.outputType)
    
    def convertToAnotherType(self, type):
        for item in self.answer:
            item['V'] *= type

    def setBonus(self, diameter, heigth):
        self.bonusHeigth = heigth
        self.bonusRadius = diameter / 2
        self.bonusVolume = self.calculateVolumeCylinder(self.bonusRadius, self.bonusHeigth, self.outputType)
    
    def calculateVolumeCylinder(self, radius, height, outputUnit):
        return (math.pi * radius * radius * height)

    def calculate(self):
        step = self.step
        pos = 0

        i = 0
        while i <= self.heigth:
            self.answer.append({
                'h': i,
                'V': self.calculateVolume(i)
            })
            pos += 1

            i += step

        # bonus volume
        for item in self.answer:
            item['h'] += self.bonusHeigth
            item['V'] += self.bonusVolume

        # remove first item
        self.answer.pop(0)

        i = self.bonusHeigth
        while i > 0:
            data = {
                'h': i,
                'V': self.bonusVolume / self.bonusHeigth * i
            }
            self.answer.insert(0, data)

            i -= step

    # @h - height of the segment
    def calculateVolume(self, h):
        return (h * self.width * self.length)

    def calculateTotalVolume(self):
        return (self.length * self.width * self.heigth * self.outputType)