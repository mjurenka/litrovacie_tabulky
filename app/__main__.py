# Standard library imports
import os
import logging
import json

# Non-standard library imports
from flask import Flask, render_template

# app imports
from app.models.eight_side_cylinder import EightSideCylinder
from app.models.spheroid_tank import SpheroidTank
from app.models.spheroid_cylinder_tank import SpheroidCylinderTank


output = {
    "0.000000000001": "km3",
    "0.000001": "hm3",
    "0.000001": "m3",
    "0.001": "dm3 / l",
    "1": "cm3",
    "1000": "mm3",
}

# heightTotal = 210
# heightPartial = 30
# widthTotal = 214
# widthPartial = 30
# length = 397
# step = 1
# outputType = 0.001

# model = EightSideCylinder(heightTotal, heightPartial, widthTotal, widthPartial, length, step, outputType)


# heightTop = 32
# heightPart = 133
# heightBottom = 45
# widthTotal = 218
# lengthLeft = 0
# lengthPart = 400
# lengthRight = 0
# step = 1
# outputType = 0.001
# model = SpheroidTank(heightTop, heightPart, heightBottom, widthTotal, lengthLeft, lengthPart, lengthRight, step, outputType)
# model.setBonus(12, 9000)

model = SpheroidCylinderTank(
    diameter=200,
    lengthLeft=34,
    lengthPart=750,
    lengthRight=34,
    step=1,
    outputType=0.001
)


model.calculate()
model.convert()
data = model.getAnswer()

with open('output.txt', 'w+') as f:
    for entry in data:
        f.write(f"{entry['h']}\t{str(entry['V']).replace('.', ',')}\n")